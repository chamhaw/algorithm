package com.hao.leetcode.concurrency.printinorder;

import org.testng.annotations.Test;


/**
 * Author: hao.zhang
 * Date: 2019-08-20 08:15
 */
public class CountDownLatch1Test extends BasePrintInOrder{
    @Test(invocationCount = 20)
    public void testCountDownLatch() {
        PrintInOrder print = new CountDownLatch1();
        super.printInThread(print);
    }
}
