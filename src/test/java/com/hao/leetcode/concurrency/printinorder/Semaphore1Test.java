package com.hao.leetcode.concurrency.printinorder;


import org.testng.annotations.Test;

/**
 * Author: hao.zhang
 * Date: 2019-08-20 07:39
 */
public class Semaphore1Test extends BasePrintInOrder{
    @Test(invocationCount = 20)
    void testSemaphorePrint() throws InterruptedException {
        PrintInOrder semaphore1 = new Semaphore1();
        super.printInThread(semaphore1);
    }
}
