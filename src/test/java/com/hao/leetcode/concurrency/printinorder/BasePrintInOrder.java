package com.hao.leetcode.concurrency.printinorder;

import java.util.concurrent.TimeUnit;

/**
 * Author: hao.zhang
 * Date: 2019-08-20 08:18
 */
public class BasePrintInOrder {
    protected void printInThread(PrintInOrder print) {
        Thread t1 = new Thread(() -> {
            try {
                print.first(() -> System.out.println("I am the first!"));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        Thread t2 = new Thread(() -> {
            try {
                print.second(() -> System.out.println("I am the second!"));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        Thread t3 = new Thread(() -> {
            try {
                print.third(() -> System.out.println("I am the third!"));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        t1.start();
        t2.start();
        t3.start();

        try {
            TimeUnit.SECONDS.sleep(2);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
