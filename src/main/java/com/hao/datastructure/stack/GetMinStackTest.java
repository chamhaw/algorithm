package com.hao.datastructure.stack;

import java.util.Random;

/**
 * Author: hao.zhang
 * Date: 2019-08-14 08:15
 */
public class GetMinStackTest {
    public static void main(String[] args) {
        System.out.println("================stack1================");
        GetMinStack1 stack1 = new GetMinStack1();
        testGetMinStack(stack1);
        System.out.println("================stack2================");
        GetMinStack2 stack2 = new GetMinStack2();
        testGetMinStack(stack2);
    }

    private static void testGetMinStack(BaseGetMinStack stack) {
        Random random = new Random();
        for (int i = 0; i < 20; i++) {
            int value = random.nextInt(100);
            System.out.println("push: " + value);
            stack.push(value);
            System.out.println("getMin:" + stack.getMin());

            if (i % 5 == 0) {
                System.out.println("stackData:" + stack.stackData);
                System.out.println("stackMin:" + stack.stackMin);
                int poped = stack.pop();
                System.out.println("pop: " + poped);
            }
        }
    }
}
