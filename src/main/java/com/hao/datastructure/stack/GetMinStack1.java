package com.hao.datastructure.stack;
/**
 * GetMinStack1
 */
public class GetMinStack1 extends BaseGetMinStack{
    public GetMinStack1() {
        super();
    }

    public void push(int num) {
        stackData.push(num);
        if (stackMin.empty() || num <= stackMin.peek()) {
            stackMin.push(num);
        }
    }

    public int pop() {
        int fromData = stackData.pop();
        if (fromData == stackMin.peek()) {
            stackMin.pop();
        }
        return fromData;
    }
}



