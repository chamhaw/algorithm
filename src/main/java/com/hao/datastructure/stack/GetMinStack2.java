package com.hao.datastructure.stack;

import java.util.Random;
import java.util.Stack;

/**
 * Author: hao.zhang
 * Date: 2019-08-14 08:09
 */
public class GetMinStack2 extends BaseGetMinStack{

    public GetMinStack2() {
      super();
    }

    public void push(int num) {
        stackData.push(num);
        if (stackMin.empty() || num <= stackMin.peek()) {
            stackMin.push(num);
            return;
        }
        int min = stackMin.peek();
        stackMin.push(min);
    }

    public int pop() {
        int fromData = stackData.pop();
        stackMin.pop();
        return fromData;
    }
}
