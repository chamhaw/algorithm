package com.hao.datastructure.stack;

import java.util.Random;
import java.util.Stack;

/**
 * Author: hao.zhang
 * Date: 2019-08-14 08:17
 */
public abstract class BaseGetMinStack {
    Stack<Integer> stackData;
    Stack<Integer> stackMin;

    public BaseGetMinStack() {
        stackData = new Stack<>();
        stackMin = new Stack<>();
    }

    public int getMin() {
        return stackMin.peek();
    }

    public abstract void push(int num);

    public abstract int pop();

}
