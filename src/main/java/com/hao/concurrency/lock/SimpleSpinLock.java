package com.hao.concurrency.lock;

import java.util.concurrent.atomic.AtomicReference;

/**
 * 自旋锁是指当一个线程尝试获取某个锁时，如果该锁已被其他线程占用，就一直循环检测锁是否被释放，而不是进入线程挂起或睡眠状态。
 * 一般都是阻塞死循环
 * 自旋锁适用于锁保护的临界区很小的情况，临界区很小的话，锁占用的时间就很短
 * @Author: hao.zhang
 * @Date: 9/6/19 11:19 AM
 */
public class SimpleSpinLock {
    private AtomicReference<Thread> owner = new AtomicReference<>();

    public void lock() {
        Thread thread = Thread.currentThread();
        while (!owner.compareAndSet(null, thread)) {
            // 检测如果期望锁持有者是空时获得锁
        }
    }
    public void unlock() {
        Thread thread = Thread.currentThread();
        while (!owner.compareAndSet(thread, null)) {
            // 只有加锁的线程才可以释放锁
        }
    }
}
