package com.hao.leetcode.concurrency.printinorder;

/**
 * Author: hao.zhang
 * Date: 2019-08-20 08:25
 */
public interface PrintInOrder {
    void first(Runnable print) throws InterruptedException;
    void second(Runnable print) throws InterruptedException;
    void third(Runnable print) throws InterruptedException;
}
