package com.hao.leetcode.concurrency.printinorder;

import java.util.concurrent.Semaphore;

/**
 * Author: hao.zhang
 * Date: 2019-08-20 07:39
 */
public class Semaphore1 implements PrintInOrder{
    private Semaphore sema1to2 = new Semaphore(0);
    private Semaphore sema2to3 = new Semaphore(0);


    public void first(Runnable printFirst) throws InterruptedException {
        // printFirst.run() outputs "first". Do not change or remove this line.
        printFirst.run();
        sema1to2.release();
    }

    public void second(Runnable printSecond) throws InterruptedException {
        sema1to2.acquire();
        // printSecond.run() outputs "second". Do not change or remove this line.
        printSecond.run();
        sema2to3.release();
    }

    public void third(Runnable printThird) throws InterruptedException {
        sema2to3.acquire();
        // printThird.run() outputs "third". Do not change or remove this line.
        printThird.run();
    }
}
