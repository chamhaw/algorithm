package com.hao.leetcode.concurrency.printinorder;

import java.util.concurrent.CountDownLatch;

/**
 * Author: hao.zhang
 * Date: 2019-08-20 08:15
 */
public class CountDownLatch1 implements PrintInOrder {
    private CountDownLatch downLatch1to2 = new CountDownLatch(1);
    private CountDownLatch downLatch2to3 = new CountDownLatch(1);

    public void first(Runnable printFirst) throws InterruptedException {
        // printFirst.run() outputs "first". Do not change or remove this line.
        printFirst.run();
        downLatch1to2.countDown();
    }

    public void second(Runnable printSecond) throws InterruptedException {
        downLatch1to2.await();
        // printSecond.run() outputs "second". Do not change or remove this line.
        printSecond.run();
        downLatch2to3.countDown();
    }

    public void third(Runnable printThird) throws InterruptedException {
        downLatch2to3.await();
        // printThird.run() outputs "third". Do not change or remove this line.
        printThird.run();
    }
}
