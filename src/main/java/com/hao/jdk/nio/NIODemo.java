package com.hao.jdk.nio;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.Channel;
import java.nio.channels.FileChannel;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.Set;

/**
 * Created by hao on 17-3-24.
 */
public class NIODemo {
  public void bufferDemo(String path){
    RandomAccessFile file = null;
    FileChannel fileChannel = null;
    try {
      file = new RandomAccessFile(path, "rw");
      fileChannel = file.getChannel();
      ByteBuffer byteBuffer = ByteBuffer.allocate(48);
      int readPosition = fileChannel.read(byteBuffer);
      while (readPosition != -1){
        byteBuffer.flip();
        while (byteBuffer.hasRemaining()){
          System.out.println((char) byteBuffer.get());
        }
        // clear()方法会清空整个缓冲区。compact()方法只会清除已经读过的数据。任何未读的数据都被移到缓冲区的起始处，
        // 新写入的数据将放到缓冲区未读数据的后面。
        byteBuffer.clear();
//        byteBuffer.compact();
        readPosition = fileChannel.read(byteBuffer);
      }
    } catch (FileNotFoundException e) {
      System.out.println("File not found.");
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }finally {
      if (fileChannel != null) {
        try {
          fileChannel.close();
        } catch (IOException e) {
          System.out.println("fileChannel failed to close.");
          e.printStackTrace();
        }
      }
      if (file != null) {
        try {
          file.close();
        } catch (IOException e) {
          System.out.println("RandomAccessFile object failed to close.");
          e.printStackTrace();
          String s = "";

        }
      }
    }
  }
  public void selectorDemo(){
    try {
      Selector selector = Selector.open();
      SocketChannel channel = SocketChannel.open();
      channel.configureBlocking(false);
      SelectionKey selectionKey = channel.register(selector, SelectionKey.OP_READ | SelectionKey.OP_ACCEPT);
      int interestSet = selectionKey.interestOps();
      boolean isInterestedAccept = (interestSet & SelectionKey.OP_ACCEPT) == SelectionKey.OP_ACCEPT;
      int readySet = selectionKey.readyOps();
      boolean isAcceptable = selectionKey.isAcceptable();

      Channel channel1 = selectionKey.channel();
      Selector selector1 = selectionKey.selector();

      Set selectedKeys = selector.selectedKeys();

      Iterator keyIterator = selectedKeys.iterator();
      while (keyIterator.hasNext()){
        SelectionKey key = (SelectionKey) keyIterator.next();
        if (key.isAcceptable()){
              //
        }else if (key.isConnectable()){
          //
        }else if (key.isReadable()){
          //
        }else if (key.isWritable()){
          //
        }
        keyIterator.remove();
      }

    } catch (IOException e) {
      e.printStackTrace();
    }
  }
  public void selectorDemo1(){
    try {
      Selector selector = Selector.open();
      SocketChannel channel = SocketChannel.open();
      channel.configureBlocking(false);
      SelectionKey selectionKey = channel.register(selector, SelectionKey.OP_READ);
      while (true){
        int readyChannels = selector.select();
        if (readyChannels == 0){
          continue;
        }
        Set selectionKeys = selector.selectedKeys();
        Iterator keyIterator = selectionKeys.iterator();
        while (keyIterator.hasNext()){
          SelectionKey key = (SelectionKey) keyIterator.next();
          if (key.isReadable()){
            // read
            SocketChannel socketChannel = (SocketChannel) key.channel();
            key.attachment();
//            socketChannel.read()
          }
          keyIterator.remove();
        }

      }
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}
